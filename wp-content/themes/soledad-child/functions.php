<?php
/*
 Soledad child theme functions and definitions
*/
function penci_soledad_child_scripts() {
    wp_enqueue_style( 'penci-soledad-parent-style', get_template_directory_uri(). '/style.css' );
}
add_action( 'wp_enqueue_scripts', 'penci_soledad_child_scripts' );
//echo get_template_directory_uri().'/soleded-child/custom_social/custom_social.php';


//require_once (get_template_directory_uri().'/soleded-child/custom_social/custom_social.php');
//include_once(get_stylesheet_directory().'/custom_social/custom_social.php');

//require_once WP_CONTENT_DIR . '/soleded-child/custom_social/custom_social.php';


if ( function_exists('register_sidebar') )
  register_sidebar(array(
    'name' => 'Social Widgetized Area',
    'before_widget' => '<div class = "widgetizedArea">',
    'after_widget' => '</div>',
    'before_title' => '<h3>',
    'after_title' => '</h3>',
  )
);

add_action( 'after_setup_theme', 'penci_soledad_theme_setup' );
if ( ! function_exists( 'penci_soledad_theme_setup' ) ) {
    function penci_soledad_theme_setup() {

        // This theme styles the visual editor with editor-style.css to match the theme style.
        add_editor_style();

        // Register navigation menu
        register_nav_menus( array(
            'main-menu'   => 'Primary Menu',
            'topbar-menu' => 'Topbar Menu',
            'footer-menu' => 'Footer Menu',
            'social-header-menu'=> 'Social Menu'
        ) );

        // Localization support
        load_theme_textdomain( 'soledad_child', get_template_directory() . '/languages' );

        // Feed Links
        add_theme_support( 'automatic-feed-links' );
        
        // Title tag
        add_theme_support( 'title-tag' );

        // Post formats - we support 4 post format: standard, gallery, video and audio
        add_theme_support( 'post-formats', array( 'standard', 'gallery', 'video', 'audio', 'link', 'quote' ) );

        // Post thumbnails
        add_theme_support( 'post-thumbnails' );
        add_image_size( 'penci-full-thumb', 1170, 0, true );
        add_image_size( 'penci-slider-thumb', 1170, 663, true );
        add_image_size( 'penci-magazine-slider', 780, 516, true );
        add_image_size( 'penci-slider-full-thumb', 1920, 800, true );
        add_image_size( 'penci-thumb', 585, 390, true );
        add_image_size( 'penci-masonry-thumb', 585, 99999, false );
        add_image_size( 'penci-thumb-square', 585, 585, true );
        add_image_size( 'penci-thumb-vertical', 480, 650, true );
        add_image_size( 'penci-thumb-small', 263, 175, true );
    }
}



function get_social_media(){ 
  $facebook_share="https://www.facebook.com/sharer/sharer.php?u=";
  $twitter_share="https://twitter.com/home?status=";
  $whatsapp_share="https://www.whatsapp.com/";
  $gen_share="http://www.sharelinkgenerator.com/";
?>
  <div class="tags-share-box hide-tags page-share has-line<?php if( get_theme_mod( 'penci_portfolio_next_prev_project' ) || get_theme_mod( 'penci_portfolio_enable_comment' ) ): echo ' no-border-bottom-portfolio'; endif;?>">
  						<div class="post-share">
  							<span class="share-title"><?php esc_html_e( 'Share', 'soledad' ); ?></span>
  							<div class="list-posts-share">

  <a target="_blank" href="<?php echo esc_url( $facebook_share ); ?>"><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/facebook.png" alt="facebook" /><span class="dt-share"><?php esc_html_e( 'Facebook', 'soledad' ); ?></span></a>
  <a target="_blank" href="<?php echo esc_url( $twitter_share ); ?>"><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/twitter.png" alt="twitter" /><span class="dt-share"><?php esc_html_e( 'Twitter', 'soledad' ); ?></span></a>
  <a target="_blank" href="<?php echo esc_url( $whatsapp_share ); ?>"><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/whatsapp.png" alt="whatsapp" /><span class="dt-share"><?php esc_html_e( 'whatsapp', 'soledad' ); ?></span></a>
  <a target="_blank" href="<?php echo esc_url( $gen_share ); ?>"><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/share.png" alt="share" /><span class="dt-share"><?php esc_html_e( 'share', 'soledad' ); ?></span></a>


  </div>
						</div>
					</div>

<?php }
add_shortcode('social-media-code','get_social_media');

function is_blog () {
    return ( is_archive() || is_author() || is_category() || is_home() || is_tag() || is_single()) ;
}



function custom_meta_box_markup($object)
{
    wp_nonce_field(basename(__FILE__), "meta-box-nonce");

    ?>
        <div>
           
            <br>

           

            <label for="meta-box-checkbox">Check Box</label>
            <?php
                $checkbox_value = get_post_meta($object->ID, "meta-box-checkbox", true);

                if($checkbox_value == "")
                {
                    ?>
                        <input name="meta-box-checkbox" type="checkbox" value="true">
                    <?php
                }
                else if($checkbox_value == "true")
                {
                    ?>  
                        <input name="meta-box-checkbox" type="checkbox" value="true" checked>
                    <?php
                }
            ?>
        </div>
    <?php  
}


function show_home_portfolio_meta_box($post)
{
    wp_nonce_field(basename(__FILE__), "meta-box-nonce");

    ?>
        <p class="l_admin-form-item">
            <label for="show_home_portfolio">Show on homepage</label>
            <?php
                $show_home_portfolio = get_post_meta($post->ID, "show_home_portfolio", true);

                if($show_home_portfolio == "")
                {
                    ?>
                        <input name="show_home_portfolio" type="checkbox" value="true">
                    <?php
                }
                else if($show_home_portfolio == "true" || $show_home_portfolio == "on")
                {
                    ?>  
                        <input name="show_home_portfolio" type="checkbox" value="true" checked>
                    <?php
                }
            ?>
        </p>
    <?php 
}

function add_home_portfolio_meta_box()
{
    add_meta_box($post->ID, "Additional Options", "show_home_portfolio_meta_box", "portfolio", "normal", "high", null);
}

add_action("add_meta_boxes", "add_home_portfolio_meta_box");

function save_home_portfolio_meta_box($post_id, $post, $update)
{
    if (!isset($_POST["meta-box-nonce"]) || !wp_verify_nonce($_POST["meta-box-nonce"], basename(__FILE__)))
        return $post_id;

    if(!current_user_can("edit_post", $post_id))
        return $post_id;

    if(defined("DOING_AUTOSAVE") && DOING_AUTOSAVE)
        return $post_id;

	$meta_box_checkbox_value = "";

    if(isset($_POST["show_home_portfolio"]))
    {
        $meta_box_checkbox_value = $_POST["show_home_portfolio"];
    }   
    update_post_meta($post_id, "show_home_portfolio", $meta_box_checkbox_value);
}

add_action("save_post", "save_home_portfolio_meta_box", 10, 3);

function get_social_media_blog(){ 
  $facebook_share="https://www.facebook.com/sharer/sharer.php?u=";
  $twitter_share="https://twitter.com/home?status=";
  $whatsapp_share="https://www.whatsapp.com/";
  $gen_share="http://www.sharelinkgenerator.com/";
?>
  <div class="tags-share-box hide-tags page-share has-line<?php if( get_theme_mod( 'penci_portfolio_next_prev_project' ) || get_theme_mod( 'penci_portfolio_enable_comment' ) ): echo ' no-border-bottom-portfolio'; endif;?>">
                        <div class="post-share">
                            <span class="share-title"><?php esc_html_e( '', 'soledad' ); ?></span>
                            <div class="list-posts-share">

  <a target="_blank" href="<?php echo esc_url( $facebook_share ); ?>"><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/facebook.png" alt="facebook" /><span class="dt-share"><?php esc_html_e( 'Facebook', 'soledad' ); ?></span></a>
  <a target="_blank" href="<?php echo esc_url( $twitter_share ); ?>"><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/twitter.png" alt="twitter" /><span class="dt-share"><?php esc_html_e( 'Twitter', 'soledad' ); ?></span></a>
  <a target="_blank" href="<?php echo esc_url( $whatsapp_share ); ?>"><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/whatsapp.png" alt="whatsapp" /><span class="dt-share"><?php esc_html_e( 'whatsapp', 'soledad' ); ?></span></a>
  <a target="_blank" href="<?php echo esc_url( $gen_share ); ?>"><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/share.png" alt="share" /><span class="dt-share"><?php esc_html_e( 'share', 'soledad' ); ?></span></a>


  </div>
                        </div>
                    </div>

<?php }
add_shortcode('social-media-code-blog','get_social_media_blog');



?>

