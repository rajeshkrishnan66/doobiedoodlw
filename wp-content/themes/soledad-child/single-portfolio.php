<?php
/**
 * Template to displaying single portfolio
 * This template be registered in this plugin
 *
 * @since 1.0
 */

get_header();

$block_style = get_theme_mod('penci_blockquote_style') ? get_theme_mod('penci_blockquote_style') : 'style-1';
?>

<?php if( ! get_theme_mod( 'penci_disable_breadcrumb' ) ): ?>
	<div class="container penci-breadcrumb single-breadcrumb">
		<span><a class="crumb" href="<?php echo esc_url( home_url('/') ); ?>">
			<?php
			if( function_exists( 'penci_get_setting' ) ) {
				echo penci_get_setting( 'penci_trans_home' );
			} else {
				esc_html_e( 'Home', 'pencidesign' );
			}
			?>
		</a></span><i class="fa fa-angle-right"></i>
		<?php 
		$penci_cats = wp_get_post_terms( get_the_ID(), 'portfolio-category' );
		if ( ! empty( $penci_cats ) ):
		?>
		<span>
			<?php
			echo '<a href="' . get_term_link( $penci_cats[0], 'portfolio-category' ) . '">' . $penci_cats[0]->name . '</a>';
			?>
		</span><i class="fa fa-angle-right"></i>
		<?php endif; ?>
		<span><?php the_title(); ?></span>
	</div>
<?php endif; ?>

<div class="container <?php if ( get_theme_mod( 'penci_portfolio_single_enable_sidebar' ) ) : ?>penci_sidebar<?php endif; ?>">
	<div id="main">
		<?php /* The loop */
		while ( have_posts() ) : the_post(); ?>
			<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
				<div class="penci-page-header">
					<h1><?php the_title(); ?></h1>
				</div>
				<div class="post-entry <?php echo 'blockquote-'. $block_style; ?>">
					<div class="portfolio-page-content">
						<?php /* Thumbnail */
						if ( has_post_thumbnail() && ! get_theme_mod( 'penci_portfolio_hide_featured_image_single' ) ) {
							echo '<div class="single-portfolio-thumbnail">';
							the_post_thumbnail( 'penci-full-thumb' );
							echo '</div>';
						}
						?>
						<div class="portfolio-detail">
							<?php the_content(); ?>
						</div>
					</div>
				</div>
				<?php if ( ! get_theme_mod( 'penci_portfolio_share_box' ) ) : ?>
				
					<?php echo do_shortcode('[social-media-code]'); ?>



				<?php endif; ?>

				<?php if ( get_theme_mod( 'penci_portfolio_next_prev_project' ) ) : ?>
					<div class="post-pagination project-pagination">
						<?php
						$next_text = 'Next Project';
						if( get_theme_mod( 'penci_portfolio_next_text' ) ): $next_text = do_shortcode( get_theme_mod( 'penci_portfolio_next_text' ) ); endif;
						$prev_text = 'Previous Project';
						if( get_theme_mod( 'penci_portfolio_prev_text' ) ): $prev_text = do_shortcode( get_theme_mod( 'penci_portfolio_prev_text' ) ); endif;
						?>
						<div class="prev-post">
							<?php previous_post_link('%link', $prev_text, $in_same_term = true, $excluded_terms = '', $taxonomy = 'portfolio-category'); ?>
						</div>
						<div class="next-post">
							<?php next_post_link('%link', $next_text, $in_same_term = true, $excluded_terms = '', $taxonomy = 'portfolio-category'); ?>
						</div>
					</div>
				<?php endif; ?>
				
				<?php if ( get_theme_mod( 'penci_portfolio_enable_comment' ) ) : ?>
					<?php comments_template( '', true ); ?>
				<?php endif; ?>
				
			</article>
		<?php endwhile; ?>
	</div>

	<?php if ( get_theme_mod( 'penci_portfolio_single_enable_sidebar' ) ) : ?><?php get_sidebar(); ?><?php endif; ?>

<?php get_footer(); ?>